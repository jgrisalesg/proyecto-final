import { Card, Col, Row, Avatar } from "antd";
import {
  EditOutlined,
  EllipsisOutlined,
  SettingOutlined,
} from "@ant-design/icons";
import React from "react";

const { Meta } = Card;

export default function Guess() {
  return (
    <div className='site-card-wrapper' >
      <Row gutter={16}>
        <Col span={5}>
          <Card
            style={{ width: 300 }}
            cover={
              <img
                alt='example'
                src='https://t4.ftcdn.net/jpg/04/91/17/03/240_F_491170369_elLGyJJ3oHIVPMXRQKk8vJSa0bA2RPDO.jpg
        '
              />
            }
            actions={[
              <SettingOutlined key='setting' />,
              <EditOutlined key='edit' />,
              <EllipsisOutlined key='ellipsis' />,
            ]}>
            <Meta
              avatar={<Avatar src='https://joeschmoe.io/api/v1/jess' />}
              title='Glasses'
              description="Is simply dummy text of the printing and typesetting industry. 
      Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
       when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
       It has survived not only five centuries."
            />
          </Card>
        </Col>
        <Col span={5}>
          <Card
            style={{ width: 300 }}
            cover={
              <img
                alt='example'
                src='https://t3.ftcdn.net/jpg/04/17/01/56/240_F_417015664_jhxcGt6neXfCPL7rmaKTVAYO12jmnuFN.jpg'
              />
            }
            actions={[
              <SettingOutlined key='setting' />,
              <EditOutlined key='edit' />,
              <EllipsisOutlined key='ellipsis' />,
            ]}>
            <Meta
              avatar={<Avatar src='https://joeschmoe.io/api/v1/jon' />}
              title='Eggs'
              description="Is simply dummy text of the printing and typesetting industry. 
      Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
       when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
       It has survived not only five centuries."
            />
          </Card>
        </Col>
      </Row>
      <div>
        {" "}
        <br></br>
      </div>
      <Row gutter={16}>
        <Col span={5}>
          <Card
            style={{ width: 300 }}
            cover={
              <img
                alt='example'
                src='https://t4.ftcdn.net/jpg/03/23/21/37/240_F_323213776_XRljuNweNOvJWzZEXZsv3QjNSQsMcaJz.jpg'
              />
            }
            actions={[
              <SettingOutlined key='setting' />,
              <EditOutlined key='edit' />,
              <EllipsisOutlined key='ellipsis' />,
            ]}>
            <Meta
              avatar={<Avatar src='https://joeschmoe.io/api/v1/random' />}
              title='Robot'
              description="Is simply dummy text of the printing and typesetting industry. 
      Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
       when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
       It has survived not only five centuries."
            />
          </Card>
        </Col>
        <Col span={5}>
          <Card
            style={{ width: 300 }}
            cover={
              <img
                alt='example'
                src='https://t3.ftcdn.net/jpg/02/26/60/42/240_F_226604200_3JCM6UThkUedcQrrnY2l7fkkEkexqiFk.jpg'
              />
            }
            actions={[
              <SettingOutlined key='setting' />,
              <EditOutlined key='edit' />,
              <EllipsisOutlined key='ellipsis' />,
            ]}>
            <Meta
              avatar={<Avatar src='https://joeschmoe.io/api/v1/random' />}
              title='Dance'
              description="Is simply dummy text of the printing and typesetting industry. 
      Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
       when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
       It has survived not only five centuries."
            />
          </Card>
        </Col>
      </Row>
    </div>
  );
}
