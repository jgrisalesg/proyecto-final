import React, { useState } from "react";
import { Layout } from "antd";
import MenuTop from "../components/AdminComponents/MenuTop";
import MenuSider from "../components/AdminComponents/MenuSider";
import "./LayoutAdmin.scss";

export default function LayoutAdmin() {
  const [menuCollapsed, setMenuCollapsed] = useState(false);
  /* Especificamos los componenetes que quremos obtener de esta Layout */
  const { Header, Content, Footer } = Layout;
  return (
    <Layout>
      <MenuSider menuCollapsed={menuCollapsed} />
      <Layout className='layout-admin'>
        <Header className='layout-admin__header'>
          <MenuTop
            menuCollapsed={menuCollapsed}
            setMenuCollapsed={setMenuCollapsed}
          />
        </Header>
        <Content className='layout-admin__content'>
          <h1>Ruras</h1>
        </Content>
        <Footer className='layout-admin__footer'>MERN react</Footer>
      </Layout>
    </Layout>
  );
}
