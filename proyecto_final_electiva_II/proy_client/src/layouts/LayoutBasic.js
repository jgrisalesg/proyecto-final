import React from "react";
import { Card, Col, Layout } from "antd";
import logo from "../assets/img/1.png";
import { Footer } from "antd/lib/layout/layout";

export default function LayoutBasic(props) {
  const { children } = props;
  return (
    <Layout>
      <div className='site-card-wrapper'>
        <Card
          style={{ width: 350 }}
          cover={<img alt='example' src={logo} />}
          title='Registro de usuario'>
          <Layout>
            <h2>Menu</h2>
            <div>{children}</div>
            <image></image>

            <Footer>
              <ul>
                <li>
                  <a
                    target='_blank'
                    href='https://github.com/JhonathanGrisales' rel="noreferrer">
                    GIHUB
                  </a>
                </li>
              </ul>
            </Footer>
          </Layout>
        </Card>
      </div>
    </Layout>
  );
}
