import React from "react";
import Logo from "../../../assets/img/logo.png";
import { Button } from "antd";
import {
  MenuUnfoldOutlined,
  PoweroffOutlined,
} from "@ant-design/icons";
import "./MenuTop.scss";

export default function MenuTop(props) {
  const { menuCollapsed, setMenuCollapsed } = props;

  return (
    <div className='menu-top'>
      <div className='menu-top__left'>
        <img className='menu-top__left-logo' src={Logo} alt='Alternate' />
        <Button type='link' onClick={() => setMenuCollapsed(!menuCollapsed)}>
          {menuCollapsed ? <MenuUnfoldOutlined /> : <MenuUnfoldOutlined />}
        </Button>
      </div>
      <div className='menu-top__right'>
        <Button type='link' onClick={() => console.log("Log off!")}>
          <PoweroffOutlined />
        </Button>
      </div>
    </div>
  );
}
